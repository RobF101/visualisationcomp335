Step 1: Clone this repository
Step 2: Run your ds-server with -v brief enabled and redirect the log to a file (say fileName.txt).
Step 3: Move the log to the visualisationcomp335/out/production/Visualisation folder.
Step 4: Run "java Visualiser fileName.txt > output.csv" (This program just prints out the values needed, you redirect these to a CSV file)
Step 5: Open the output.csv file in excel...