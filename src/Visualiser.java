import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Visualiser {

    HashMap<String, ArrayList<SimEvent>> simEvents = new HashMap<>();
    ArrayList<Server> servs = new ArrayList<>();

    public Visualiser(String file) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {

                if (line.charAt(0) == 't') {
                    line = line.replace('#',' ');
                    line =line.replaceAll(" on "," ");
                    String[] a = line.split(" ");
                    ArrayList<String> lineSplit = new ArrayList<>();
                    for (int i = 0; i < a.length; i++) {
                        if (a[i].length() != 0) {
                            if (a[i].charAt(0) != '(') {
                                lineSplit.add(a[i]);
                            }
                        }
                    }
                    if (simEvents.get(lineSplit.get(lineSplit.size() - 2)) == null) {
                        simEvents.put(lineSplit.get(lineSplit.size() - 2), new ArrayList<>());
                    }
                    simEvents.get(lineSplit.get(lineSplit.size() - 2)).add(new SimEvent(lineSplit));
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        ArrayList<Integer> times = new ArrayList<>();
        for (String key : simEvents.keySet()) {
            for (int i = 0; i < simEvents.get(key).size(); i++) {
                SimEvent sE = simEvents.get(key).get(i);
                if (!times.contains(sE.getTime())) {
                    times.add(sE.getTime());
                }
            }
        }
        Collections.sort(times);
        for (String key : simEvents.keySet()) {
            for (int i = 0; i < simEvents.get(key).size(); i++) {
                SimEvent sE = simEvents.get(key).get(i);
                int index = sExists(sE.getSerID(), sE.getsType());
                if (index == -1) {
                    Server s = new Server(sE.getsType(), sE.getSerID());
                    s.add(sE);
                    servs.add(s);
                } else {
                    servs.get(index).add(sE);
                }
            }
        }
        System.out.print("times,times,times,");
        for (int i = 0; i < times.size(); i++) {
            System.out.print(times.get(i) + ",");
        }
        System.out.print("\n");
        System.out.println("ServerType,ServerID,jobID,");
        for (int i = 0; i < servs.size(); i++) {
            System.out.print(servs.get(i).toString(times));
        }
    }

    int sExists(int id, String t) {
        for (int i = 0; i < servs.size(); i++) {
            if (servs.get(i).getId() == id && servs.get(i).getType().equals(t)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        //new Visualiser("C:\\Users\\Ghost\\Documents\\2019 3rdYrUni\\Semester 1\\COMP335\\Logs\\configs (wf)\\config_simple3.xml.your.log").start();
        if (args.length != 1) {
            System.err.println("Invalid format, use \"java Visualiser logFileName.txt\"");
            return;
        }
        new Visualiser(args[0]).start();
    }
}
