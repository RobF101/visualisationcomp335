import java.util.ArrayList;

public class Job {
    private int jID;
    private int schT;
    private int runT;
    private int compT;

    Job(int s, int r, int c, int id) {
        this.compT = c;
        this.runT = r;
        this.schT = s;
        this.jID = id;
    }

    public String toString(ArrayList<Integer> t) {
        String ret = ",job," + jID + ",";
        String mode = "notRunning";
        for (int i = 0; i < t.size(); i++) {
            if (schT == t.get(i)) {
                mode = "sch";
            } else if (runT == t.get(i)) {
                mode = "run";
            }
            if (compT == t.get(i)) {
                mode = "comp";
            }
            if (compT == t.get(i) && runT == t.get(i)) {
                mode = "rComp";
            }
            if (schT == t.get(i) && runT == t.get(i)) {
                mode = "schR";
            }
            if (mode.equals("notRunning")) {
                ret = ret + ",";
            } else if (mode.equals("sch")) {
                ret = ret + "SCH,";
                mode="wait";
            }else if (mode.equals("wait")) {
                ret = ret + "WAIT,";
            } else if (mode.equals("run")) {
                ret = ret + "RUN,";
            } else if (mode.equals("comp")) {
                ret = ret + "COMP";
                return ret;
            } else if (mode.equals("schR")) {
                ret = ret + "SCH/RUN,";
                mode="run";
            }
        }
        return ret;
    }
}
