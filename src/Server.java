import java.util.ArrayList;
import java.util.HashMap;

public class Server {
    private String type;
    private int id;
    private ArrayList<SimEvent> sEList = new ArrayList<>();
    private ArrayList<Job> jobList = new ArrayList<>();
    private String sch;
    private String run;
    private String comp;

    Server(String t, int i) {
        this.type = t;
        this.id = i;
    }

    public void add(SimEvent sE) {
        this.sEList.add(sE);
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public ArrayList<SimEvent> getEList() {
        return sEList;
    }

    public String getSch() {
        return sch;
    }

    public String getRun() {
        return run;
    }

    public String getComp() {
        return comp;
    }

    public String toString(ArrayList<Integer> t) {
        String ret = this.type + "," + this.id + "\n";
        generateJobList();
        for(int i=0;i<jobList.size();i++){
            ret=ret+jobList.get(i).toString(t)+"\n";
        }
        return ret;// + this.sch + "\n" + this.run + "\n" + this.comp + "\n"
    }

    private void generateJobList() {
        ArrayList<Integer> jobIDs = new ArrayList<>();
        for (int i = 0; i < sEList.size(); i++) {
            if (!jobIDs.contains(sEList.get(i).getJobID())) {
                jobIDs.add(sEList.get(i).getJobID());
            }
        }
        for (int i = 0; i < jobIDs.size(); i++) {
            int id = jobIDs.get(i);
            int sch = 0;
            int run = 0;
            int comp = 0;
            for (int j = 0; j < sEList.size(); j++) {
                if (sEList.get(j).getJobID() == id) {
                    if (sEList.get(j).geteType().equals("COMPLETED")) {
                        comp = sEList.get(j).getTime();
                    } else if (sEList.get(j).geteType().equals("SCHEDULED")) {
                        sch = sEList.get(j).getTime();
                    } else if (sEList.get(j).geteType().equals("RUNNING")) {
                        run = sEList.get(j).getTime();
                    }
                }
            }
            jobList.add(new Job(sch, run, comp, id));
        }
    }
}
