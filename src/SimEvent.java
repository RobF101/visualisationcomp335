import java.util.ArrayList;

public class SimEvent {
    private int time;
    private int jobID;
    private int serID;
    private String sType;
    private String eType;

    SimEvent(ArrayList<String> s){
        this.eType=s.get(s.size()-1);
        this.sType=s.get(s.size()-2);
        this.serID=Integer.parseInt(s.get(s.size()-5));
        this.jobID=Integer.parseInt(s.get(3));
        this.time=Integer.parseInt(s.get(1));
    }

    public int getTime() {
        return time;
    }

    public int getJobID() {
        return jobID;
    }

    public int getSerID() {
        return serID;
    }

    public String getsType() {
        return sType;
    }

    public String geteType() {
        return eType;
    }

    @Override
    public String toString() {
        return "SimEvent{" +
                "time=" + time +
                ", jobID=" + jobID +
                ", serID=" + serID +
                ", sType='" + sType + '\'' +
                ", eType='" + eType + '\'' +
                '}';
    }
}
