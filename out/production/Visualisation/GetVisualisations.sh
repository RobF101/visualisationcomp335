#!/bin/bash
# to kill multiple runaway processes, use 'pkill runaway_process_name'
# For the Java implementation, use the following format: ./tests1.sh your_client.class [-n]
configDir="./logs"

if [ ! -d $configDir ]; then
	echo "No $configDir found!"
	exit
fi

yourClient=$1
#options=$1;
shift

for conf in $configDir/*.log; do
	echo $conf
	echo "running your implementation ($yourClient)..."
	if [ -f $yourClient ] && [[ $yourClient == *".class" ]]; then
		java $(sed 's/\.class//' <<< $yourClient) $conf > $conf.csv
	fi
	sleep 1
	echo ============
	sleep 1
done

echo "done"

